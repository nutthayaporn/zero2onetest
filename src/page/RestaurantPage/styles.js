import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
  },
  infoBlock: {
    paddingVertical: 20,
  },
  textTopic: {
    marginBottom: 10,
  },
  textRestaurant: {
    paddingVertical: 3,
  },
});

export default styles;
