import React, {useEffect, useState} from 'react';
import {ScrollView, View, Text} from 'react-native';
import _ from 'lodash';

import {fetchRestaurantNearBy} from '../../services';

import styles from './styles';

const RestaurantPage = () => {
  const [restaurants, setRestaurants] = useState(null);

  useEffect(() => {
    const init = async () => {
      const resultFirst = await fetchRestaurantNearBy();
      if (_.get(resultFirst, 'status') !== 'success') {
        return;
      }
      const nextPageToken = _.get(resultFirst, 'nextPageToken');
      const restaurantFirst20Lists = _.get(resultFirst, 'data');

      const resultSecond = await fetchRestaurantNearBy(nextPageToken);
      if (_.get(resultSecond, 'status') !== 'success') {
        return;
      }
      const restaurantSecond20Lists = _.get(resultSecond, 'data');

      setRestaurants([...restaurantFirst20Lists, ...restaurantSecond20Lists]);
    };

    init();
  }, []);

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.infoBlock}>
          <Text style={styles.textTopic}>Restaurant Near by Bangsue</Text>
          <View style={styles.allRestaurantsBlock}>
            {_.map(restaurants, (restaurant, index) => {
              const name = _.get(restaurant, 'name');
              const number = index + 1;
              return (
                <View style={styles.restaurantBlock} key={index}>
                  <Text
                    style={styles.textRestaurant}>{`${number}. ${name}`}</Text>
                </View>
              );
            })}
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default RestaurantPage;
