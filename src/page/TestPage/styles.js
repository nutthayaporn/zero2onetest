import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
  },
  containerForm: {
    marginTop: 20,
  },
  inputStyle: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 3,
  },
  buttonBlock: {
    marginTop: 10,
  },
  containerResult: {
    marginTop: 20,
  },
  seqResultBlock: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 8,
  },
  textResult: {},
});

export default styles;
