import React, {useState} from 'react';
import {View, Text, TextInput, Button} from 'react-native';
import _ from 'lodash';

import styles from './styles';

const TestPage = () => {
  const [inputValue, setInputValue] = useState(null);
  const [seqResult, setSeqResult] = useState(null);

  const findValueOfSeq = number => {
    const collections = [];
    _.reduce(
      _.times(number),
      (result, v, key) => {
        const sum = result + 2 * key;
        collections.push(sum);
        return sum;
      },
      3,
    );
    return collections;
  };

  const onChangeText = value => {
    setInputValue(value);
    if (!value) {
      setSeqResult(null);
    }
  };

  const onPressButton = () => {
    if (!inputValue) {
      return;
    }
    const collections = findValueOfSeq(inputValue);
    setSeqResult(collections);
  };

  return (
    <View style={styles.container}>
      <View style={styles.containerForm}>
        <View styles={styles.inputBlock}>
          <TextInput
            style={styles.inputStyle}
            onChangeText={value => onChangeText(value)}
            placeholder="Please input a number"
            value={inputValue}
          />
        </View>
        <View style={styles.buttonBlock}>
          <Button
            style={styles.buttonStyle}
            onPress={onPressButton}
            title="Submit"
            color="#13999B"
          />
        </View>
      </View>
      {seqResult && (
        <View style={styles.containerResult}>
          <Text style={styles.textResult}>Result :</Text>
          <View style={styles.seqResultBlock}>
            {_.map(seqResult, (num, index) => {
              const isLast = _.size(seqResult) === index + 1;
              return <Text key={index}>{`${num}${isLast ? '' : ', '}`}</Text>;
            })}
          </View>
        </View>
      )}
    </View>
  );
};

export default TestPage;
