import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {TestPage, RestaurantPage} from './page';

const Tab = createBottomTabNavigator();
const TestStack = createStackNavigator();
const RestaurantStack = createStackNavigator();

const TestPageStack = () => {
  return (
    <TestStack.Navigator>
      <TestStack.Screen
        name="Test"
        component={TestPage}
        options={{
          tabBarLabel: 'Test',
        }}
      />
    </TestStack.Navigator>
  );
};

const RestaurantPageStack = () => {
  return (
    <RestaurantStack.Navigator>
      <RestaurantStack.Screen
        name="Restaurant"
        component={RestaurantPage}
        options={{
          tabBarLabel: 'Restaurant',
        }}
      />
    </RestaurantStack.Navigator>
  );
};

const App = () => {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused}) => {
            let iconName;

            if (route.name === 'Test') {
              iconName = 'calculator-variant';
            } else if (route.name === 'Restaurant') {
              iconName = 'silverware-fork-knife';
            }

            return (
              <Icon
                name={iconName}
                size={20}
                color={focused ? '#ffffff' : '#13999B'}
              />
            );
          },
        })}
        tabBarOptions={{
          activeBackgroundColor: '#13999B',
          activeTintColor: '#ffffff',
          inactiveTintColor: '#13999B',
          labelStyle: {textTransform: 'uppercase'},
        }}>
        <Tab.Screen name="Test" component={TestPageStack} />
        <Tab.Screen name="Restaurant" component={RestaurantPageStack} />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default App;
