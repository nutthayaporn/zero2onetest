import _ from 'lodash';

const fetchRestaurantNearBy = async token => {
  const apiKey = 'AIzaSyA93NnY0robI3VB6pyDEG6g7J3SGsHQcDI';
  const location = '13.8234866,100.5081204';
  const radius = 400;
  let apiUrl = `https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${location}&radius=${radius}&types=restaurant&key=${apiKey}`;

  if (token) {
    apiUrl = `https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${location}&radius=${radius}&types=restaurant&key=${apiKey}&pagetoken=${token}`;
  }

  try {
    const fetchResult = await fetch(apiUrl).then(response => response.json());
    return {
      status: 'success',
      data: _.get(fetchResult, 'results'),
      nextPageToken: _.get(fetchResult, 'next_page_token'),
    };
  } catch (error) {
    return {status: 'error', data: [], message: error.message};
  }
};

export {fetchRestaurantNearBy};
